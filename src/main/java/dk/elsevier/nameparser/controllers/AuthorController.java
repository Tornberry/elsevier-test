package dk.elsevier.nameparser.controllers;

import dk.elsevier.nameparser.models.Author;
import dk.elsevier.nameparser.services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AuthorController {

    AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    /**
     * Parses given name string into Author object
     *
     * @param name The name of an author in string format
     * @return The parsed author as JSON
     */
    @GetMapping("/author/single")
    public ResponseEntity parseNameToAuthor(@RequestParam String name) {
        try {
            return ResponseEntity.ok(authorService.parseStringToAuthor(name));
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body("Error parsing author: " + exception.getMessage());
        }
    }

    /**
     * Parses given names string into Author list
     *
     * @param names The names of authors in string format
     * @return The parsed authors as JSON
     */
    @GetMapping("/author/multiple")
    public ResponseEntity parseNamesToAuthors(@RequestParam String names) {
        try {
            return ResponseEntity.ok(authorService.parseStringToAuthorList(names));
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body("Error parsing authors: " + exception.getMessage());
        }
    }
}
