package dk.elsevier.nameparser.services;

import dk.elsevier.nameparser.models.Author;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class AuthorService {

    public Author parseStringToAuthor(String name) throws Exception {
        if (name.contains(",")) {
            String[] splitName = name.split(", ");
            String firstname = splitName[1];
            String lastname = splitName[0];

            return new Author(firstname, lastname);
        } else {
            String[] splitName = name.split(" ");
            String[] nameArrayWithoutLast = Arrays.copyOf(splitName, splitName.length-1);
            String firstname = String.join(" ", nameArrayWithoutLast);
            String lastname = splitName[splitName.length-1];

            return new Author(firstname, lastname);
        }
    }

    public List<Author> parseStringToAuthorList(String names) {
        String[] namesArray = names.split(", ");
        List<Author> authorList = new ArrayList<>();

        Arrays.stream(namesArray).forEach(name -> {
            try {
                authorList.add(parseStringToAuthor(name));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        return authorList;
    }

}
