package dk.elsevier.nameparser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NameparserApplication {

	public static void main(String[] args) {
		SpringApplication.run(NameparserApplication.class, args);
	}

}
