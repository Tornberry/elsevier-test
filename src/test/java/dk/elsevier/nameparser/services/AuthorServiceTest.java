package dk.elsevier.nameparser.services;

import dk.elsevier.nameparser.models.Author;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.atIndex;
import static org.junit.jupiter.api.Assertions.*;
import org.assertj.core.api.Assertions;

import java.util.List;

class AuthorServiceTest {

    @Test
    void parseStringToPerson() throws Exception {
        AuthorService authorService = new AuthorService();

        assertThat(authorService.parseStringToAuthor("John Doe"))
                .extracting("firstname", "lastname").containsExactly("John", "Doe");

        assertThat(authorService.parseStringToAuthor("Doe, John"))
                .extracting("firstname", "lastname").containsExactly("John", "Doe");

        assertThat(authorService.parseStringToAuthor("Hans-Christian Jensen"))
                .extracting("firstname", "lastname").containsExactly("Hans-Christian", "Jensen");

        assertThat(authorService.parseStringToAuthor("H-C Jensen"))
                .extracting("firstname", "lastname").containsExactly("H-C", "Jensen");

        assertThat(authorService.parseStringToAuthor("P. H. Kristensen"))
                .extracting("firstname", "lastname").containsExactly("P. H.", "Kristensen");

        assertThat(authorService.parseStringToAuthor("Kristensen, P. H."))
                .extracting("firstname", "lastname").containsExactly("P. H.", "Kristensen");

        assertThat(authorService.parseStringToAuthor("Peter Hans Kristensen"))
                .extracting("firstname", "lastname").containsExactly("Peter Hans", "Kristensen");

        assertThat(authorService.parseStringToAuthor("Peter H. Kristensen"))
                .extracting("firstname", "lastname").containsExactly("Peter H.", "Kristensen");
    }

    @Test
    void parseStringToPeopleList() {
        AuthorService authorService = new AuthorService();

        List<Author> authors = authorService.parseStringToAuthorList("H-C Jensen, Peter Hans Kristensen, John Doe");

        assertThat(authors).hasSize(3);

        assertThat(authors.get(0)).extracting("firstname", "lastname").containsExactly("H-C", "Jensen");
        assertThat(authors.get(1)).extracting("firstname", "lastname").containsExactly("Peter Hans", "Kristensen");
        assertThat(authors.get(2)).extracting("firstname", "lastname").containsExactly("John", "Doe");
    }
}