# Elsevier name parser

This is the submission for the Elsevier test, in the form of a Spring project.

To test, boot up the project and call the two available endpoints:

For parsing of multiple names: 


- For single name: GET https://localhost:8080/api/author/single?name=Kristensen, P. H.
- For multiple names: GET https://localhost:8080/api/author/multiple?names=H-C Jensen, Peter Hans Kristensen, John Doe
